#!/bin/sh

# during execution return error if any of the lines fail
set -e

# substitute environment variables, then specify where it will out put >
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# this is the default location where nginx expects to finds its variables

# run nginx with the daemon off (background)
# docker recommended to run with app in foreground
# this way output is printed in docaker output
nginx -g 'daemon off;'


